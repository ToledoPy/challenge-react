import { useSelector, useDispatch } from 'react-redux';
export const riskLevels=[
    [80, 20, 0, 0, 0 ],
    [70, 15, 15, 0, 0 ],
    [60, 15, 15, 10, 0 ],
    [50, 20, 20, 10, 0 ],
    [40, 20, 20, 20, 0 ],
    [35, 25, 5, 30, 5 ],
    [20, 25, 25, 25, 5 ],
    [10, 20, 40, 20, 10 ],
    [5, 15, 40, 25, 15 ],
    [0, 5, 25, 30, 40 ],
];

export const labelLevel=['Bonds', 'Large Cap', 'Mid Cap', 'Foreign', 'Small Cap'];
export const useRiskLevel = () => {
const riskLevel = useSelector((state) => state.riskLevel);
const dispatch = useDispatch();
const setRiskLevel = (value) =>
    dispatch({
        type: 'SET_RISK_LEVEL',
        payload: value
    });

    return { riskLevel, setRiskLevel }
}

export const printValue=(value, type)=>{
    if(value% 1 !== 0){
        value=value.toFixed(2);
    }
    if(type=='WITH_OPERATOR' && value>=0){
        return '+ '+value;
    }
    return value;
}
