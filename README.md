# FrontEnd Engineer - ReactJS Challenge C

### Installation Guide

- Clone repository or download the zip and unzip
- Go to folder with terminal
- Run `npm install` 
- Run `npm run dev`
- Go to  [http://localhost:3000/](http://localhost:3000/) at your browser



### For testing
- Go to folder with terminal
- Run `npm run test` 

