import App from "../../pages/index";
import Calculator from "../../pages/calculator";

import { render, fireEvent, screen } from '../../test-utils'

describe("App", () => {
    it("renders without crashing", () => {
        render(<App />);
        expect(
            screen.getByRole("heading", { name: "Financial Advisor" })
          ).toBeInTheDocument();
      });
    it("renders Correctly", () => {
        render(<App />);
        expect(
            screen.getByRole("button", { name: "Continue" })
            ).toBeInTheDocument();
    });
});

describe("Calculate", () => {
    it("renders without crashing", () => {
        render(<Calculator />, {preloadedState:{riskLevel:0}});
        expect(
            screen.getByRole("heading", { name: "Financial Advisor" })
          ).toBeInTheDocument();
      });

      it("renders Risk Level 1", () => {
        render(<Calculator />, {preloadedState:{riskLevel:0}});
        expect(
            screen.getByRole("heading", { name: "Risk Level 1" })
          ).toBeInTheDocument();
      });
      it("renders Risk Level 2", () => {
        render(<Calculator />, {preloadedState:{riskLevel:1}});
        expect(
            screen.getByRole("heading", { name: "Risk Level 2" })
          ).toBeInTheDocument();
      });
      it("renders Risk Level 3", () => {
        render(<Calculator />, {preloadedState:{riskLevel:2}});
        expect(
            screen.getByRole("heading", { name: "Risk Level 3" })
          ).toBeInTheDocument();
      });
      it("renders Risk Level 4", () => {
        render(<Calculator />, {preloadedState:{riskLevel:3}});
        expect(
            screen.getByRole("heading", { name: "Risk Level 4" })
          ).toBeInTheDocument();
      });
      it("renders Risk Level 5", () => {
        render(<Calculator />, {preloadedState:{riskLevel:4}});
        expect(
            screen.getByRole("heading", { name: "Risk Level 5" })
          ).toBeInTheDocument();
      });
});
