import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '../styles.module.css';

const Nav = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/" style={{'width':'100%'}}>
            <img src={'assets/images/home.ico'} alt="home" height="60" width="60" className="align-top" ></img>
            <h1 className={styles.titleNav}>Financial Advisor</h1>
          </Navbar.Brand>
        </Container>
      </Navbar>
    </>
  )
}

export default Nav
