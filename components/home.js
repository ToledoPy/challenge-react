import Nav from './nav';
import styles from '../styles.module.css'
import { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Link from 'next/link';
import Head from 'next/head'


import {riskLevels, useRiskLevel} from '../utils/advisor';
const chartLogo = '/assets/images/chartlogo.jpg';
const donutLogo = '/assets/images/donutlogo.png';

const calculateData = (level) => {
    if(level !== null && level <= 9 ){
        return [
            {
                y: riskLevels[level][0],
                color: "#1f77b4",
                name: "Bonds"
            }, 
            {
                y: riskLevels[level][1],
                color: "#aec7e8",
                name: "Large Cap"
            }, 
            {
                y: riskLevels[level][2],
                color: "#ff7f10",
                name: "Mid Cap"
            }, 
            {
                y: riskLevels[level][3],
                color: "#ffbb78",
                name: "Foreing"
            }, 
            {
                y: riskLevels[level][4],
                color: "#2ca02c",
                name: "Small Cap"
            }, 
        ];
    }
    return [
        {
            y: 20,
            color: "#1f77b4",
            name: "Select Level"
        }, 
        {
            y: 20,
            color: "#aec7e8",
            name: "Select Level"
        }, 
        {
            y: 20,
            color: "#ff7f10",
            name: "Select Level"
        }, 
        {
            y: 20,
            color: "#ffbb78",
            name: "Select Level"
        }, 
        {
            y: 20,
            color: "#2ca02c",
            name: "Select Level"
        }, 
    ];
}

export default function Home() {

    const [chart, setChart] = useState(false);
    const { riskLevel, setRiskLevel } = useRiskLevel();
    var data = calculateData(riskLevel);
    const options = {
        chart: {
            animation: false,
            type: 'pie',
            backgroundColor: null
        },
        tooltip:{
            enabled:true,
            formatter: function() {
                return '<strong>'+this.y + ' %</strong>';
            }
        },
        title: {
          text: `INVESTMENT <br> PORTFOLIO`,
          verticalAlign: 'middle',
          floating: true
        },
        plotOptions: {
            pie: {
                animation: {
                    duration: 750,
                    easing: 'easeOutQuad'
                },
                shadow: false,
                center: ['50%', '50%'],
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                },
               
            },
            series: {
                animation: {
                    duration: 750,
                    easing: 'easeOutQuad'
                }
            }
        },
        series: [{
            animation: {
                duration: 750,
                easing: 'easeOutQuad'
            },
            name: 'Percent',
            data: data,
            size: '90%',
            innerSize: '55%',
            dataLabels: {
                formatter: function () {
                    return this.y > 1 ? this.point.name: null;
                },
                color: '#ffffff',
                distance: -30,
            }
        }]
      }
    
    return (
        <>
        <Head>
            <title>Financial Advisor</title>
        </Head>
        <Nav />
        <Container>
            <h3 className={styles.subTitle}>
                Please Select A Risk Level For Your Investment Portfolio 
            </h3>
            <Row xs="11" className="justify-content-md-center">
              
                {riskLevels && riskLevels.map((level, index) => ( 
                    <Col key={index} style={{'padding': '5px'}}>
                        <Button variant={riskLevel==index?"success":"outline-info"} className={styles.ButtonAux} size="lg"  onClick={() => setRiskLevel(index)} >{index+1}</Button>
                    </Col>
                ))}
                <Col style={{'padding': '5px'}}>
                        <Link href="/calculator">
                            <a>
                                <Button variant="primary" size="lg" disabled={riskLevel===null} className={styles.ButtonAux} > Continue </Button>
                            </a>
                        </Link>
                </Col>
            </Row>
            <Row>
                
                <Col xs={{ span: 0, offset: 10 }} md={{ span: 0, offset: 11 }} >
                    <div style={{'marginTop': '10px', 'cursor':'pointer'}}>
                        <img src={chart ? chartLogo : donutLogo} alt="chart" height="45" width="50" onClick={() => setChart(!chart)} className={styles.imagChart}></img>
                    </div>
                </Col>
            </Row>
            { !chart ? (
                <Row className={styles.mt20} >
                    <Table bordered>
                        <thead>
                            <tr>
                                <th>Risk</th>
                                <th>Bonds %</th>
                                <th>Larg Cap %</th>
                                <th>Mid Cap %</th>
                                <th>Foreign %</th>
                                <th>Small Cap %</th>
                            </tr>
                        </thead>
                        <tbody>
                            {riskLevels && riskLevels.map((level, index) => ( 
                                <tr className={riskLevel==index ? styles.selected : null } key={index}>
                                    <td >{ index+1 }</td>
                                    {level.map((detail, subindex) => (
                                        <td key={subindex}>{detail}</td>
                                    ))}
                                </tr>
                            ))}
                            
                        </tbody>
                    </Table>
                </Row>
            ):(
                <Row>
                    <div>
                        <HighchartsReact
                            highcharts={Highcharts}
                            options={options}
                        />
                    </div>
                </Row>
            )}
        </Container>
        
        </>
    );
}