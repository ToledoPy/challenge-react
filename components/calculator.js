import Nav from './nav';
import { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import styles from '../styles.module.css';
import Router from 'next/router';
import Head from 'next/head'


import {riskLevels, useRiskLevel, labelLevel, printValue} from '../utils/advisor';


export default function Home() {

    const { riskLevel } = useRiskLevel();
    if(riskLevel == null){
        typeof window !== 'undefined' && process.browser && Router.push('/');
        return true;
    }
    const riskLevelSelected = riskLevels[riskLevel];
    const [current, setCurrent] = useState(['','','','','']);
    const [diference, setDiference] = useState([null,null,null,null,null]);
    const [newAmount, setNewAmount] = useState([null,null,null,null,null]);
    const [error, setError] = useState(false);
    const [message, setMessage] = useState(null);

    const calculateBalance= () => {
        var statusError=false;
        current.map(value => {
            if(isNaN(value) || value <0 || value=='' || /\s/g.test(value)){
                statusError=true;
            }
        });
        if(current.length!==5){
            statusError=true;
        }
        setError(statusError);
        if(statusError){
            setDiference([null,null,null,null,null]);
            setNewAmount([null,null,null,null,null]);
            setMessage([<p key="2">Please use only positive digits or zero when entering current amounts.<br></br>Please enter all inputs correctly.</p>]);
        }else{
            const total = current.reduce((a,b) => parseInt(a)+parseInt(b));
            var newAmount=[];
            var diference=[];
            var overflow=[];
            var deficit=[];
            var message=[];
            var liCounter=0;
            current.map((value, index) => {
                const expectedValue=(total/100)*riskLevelSelected[index];
                newAmount[index]=expectedValue;
                diference[index]=(value-expectedValue)*-1;
                if(diference[index]>0){
                    deficit[index]=Math.abs(diference[index]);
                }
                if(diference[index]<0){
                    overflow[index]=Math.abs(diference[index]);
                }
            });
            overflow.map((overValue, overIndex) => {
                if(overValue>0){
                    deficit.map((defValue, defIndex) => {
                        liCounter++;
                        if(defValue>0){
                            if(defValue<=overValue){
                                deficit[defIndex]=0;
                                overValue=overValue-defValue;
                                message.push(<li key={liCounter}>Transfer $ {printValue(defValue)} from {labelLevel[overIndex]} to {labelLevel[defIndex]}</li>);
                            }else{
                                if(defValue>overValue){
                                    const leak=defValue-overValue;
                                    overflow[defIndex]=0;
                                    deficit[defIndex]=leak;
                                    message.push(<li key={liCounter}>Transfer $ {printValue(overValue)} from {labelLevel[overIndex]} to {labelLevel[defIndex]}</li>);
                                }
                            }
                        }

                    });
                }
            });
            setMessage(message);
            setNewAmount(newAmount);
            setDiference(diference);
        }
    }

    const updateCurrent=(val,pos) => {
        const newArray=[...current];
        newArray[pos]=val;
        setCurrent(newArray);
    }
    return (
        <>
        <Head>
            <title>Financial Advisor | Personalized Portfolio</title>
        </Head>
        <Nav />
        <Container>
            <h3 className={styles.subTitle}>
                Personalized Portfolio
            </h3>
            <h4>
                Risk Level {riskLevel+1}
            </h4>
                <Row>
                    <Table bordered>
                        <thead>
                            <tr>
                                <th>Bonds</th>
                                <th>Larg Cap</th>
                                <th>Mid Cap</th>
                                <th>Foreign</th>
                                <th>Small Cap</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{ riskLevelSelected[0] } %</td>
                                <td>{ riskLevelSelected[1] } %</td>
                                <td>{ riskLevelSelected[2] } %</td>
                                <td>{ riskLevelSelected[3] } %</td>
                                <td>{ riskLevelSelected[4] } %</td>
                            </tr>
                        </tbody>
                    </Table>
                </Row>
            <Row>
                <h4>Please Enter Your Current Portfolio <Button variant="primary" size="lg" onClick={calculateBalance} style={{'float': 'right'}}> Rebalance </Button></h4>
            </Row>
            <Row>
            <Table bordered>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Current Amount</th>
                            <th>Difference</th>
                            <th>New Amount</th>
                            <th>Recommended Transfers</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Bonds $:</td>
                            <td><input className="form-control" tabIndex="1" value={current[0]} onChange={(e)=>{updateCurrent(e.target.value, 0)}}></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':diference[0]<0?'red':'green'}} value={(diference[0] !==null) ? printValue(diference[0],'WITH_OPERATOR'): ''} readOnly></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':'blue'}} value={(newAmount[0] !==null) ? printValue(newAmount[0]): ''} readOnly></input></td>
                            <td rowSpan={5} style={{'color':error?'red':'black'}}>{message}</td>
                        </tr>
                        <tr>
                            <td>Large Cap $:</td>
                            <td><input className="form-control" tabIndex="2" value={current[1]} onChange={(e)=>{updateCurrent(e.target.value, 1)}}></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':diference[1]<0?'red':'green'}} value={(diference[1] !==null) ? printValue(diference[1],'WITH_OPERATOR'): ''} readOnly></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':'blue'}} value={(newAmount[1] !==null) ? printValue(newAmount[1]): ''} readOnly></input></td>
                        </tr>
                        <tr>
                            <td>Mid Cap $:</td>
                            <td><input className="form-control" tabIndex="3" value={current[2]} onChange={(e)=>{updateCurrent(e.target.value, 2)}}></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':diference[2]<0?'red':'green'}} value={(diference[2] !==null) ? printValue(diference[2],'WITH_OPERATOR'): ''} readOnly></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':'blue'}} value={(newAmount[2] !==null) ? printValue(newAmount[2]): ''} readOnly></input></td>
                        </tr>
                        <tr>
                            <td>Foreign $:</td>
                            <td><input className="form-control" tabIndex="4" value={current[3]} onChange={(e)=>{updateCurrent(e.target.value, 3)}}></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':diference[3]<0?'red':'green'}} value={(diference[3] !==null) ? printValue(diference[3],'WITH_OPERATOR'): ''} readOnly></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':'blue'}} value={(newAmount[3] !==null) ? printValue(newAmount[3]): ''} readOnly></input></td>
                        </tr>
                        <tr>
                            <td>Small Cap $:</td>
                            <td><input className="form-control" tabIndex="5" value={current[4]} onChange={(e)=>{updateCurrent(e.target.value, 4)}}></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':diference[4]<0?'red':'green'}} value={(diference[4] !==null) ? printValue(diference[4],'WITH_OPERATOR'): ''} readOnly></input></td>
                            <td><input className="form-control" tabIndex="-1" style={{'color':'blue'}} value={(newAmount[4] !==null) ? printValue(newAmount[4]): ''} readOnly></input></td>
                        </tr>
                    </tbody>
                </Table>
            </Row>
            
        </Container>
        
        </>
    );
}