import React from 'react'
import { render as rtlRender } from '@testing-library/react'
import { configureStore } from '@reduxjs/toolkit'
import { Provider } from 'react-redux'
const riskLevelReducer = (state = {riskLevel:null}, action) => {
    switch (action.type) {
      case 'SET_RISK_LEVEL':
        return {
          ...state,
          riskLevel: action.payload,
        }
      default:
        return state
    }
  }

function render(
  ui,
  {
    preloadedState,
    store = configureStore({ reducer: { riskLevel: riskLevelReducer }, preloadedState }),
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

// re-export everything
export * from '@testing-library/react'
// override render method
export { render }